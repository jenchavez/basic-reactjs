// Home.tsx
import React, { ChangeEvent } from "react";
import TodoForm from "./components/TodoForm";
import TodoDisplay from "./components/TodoDisplay";
import { IProp, IState, ITodo } from "./type";
import "./style.css";

class Home extends React.Component<IProp, IState> {
  constructor(props: IProp) {
    super(props);
    this.state = {
      temp_input: "",
      temp_message: "",
      todo: [],
      showDataTable: false
    };
  }

  inputHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    this.setState({ temp_input: value });
  };

  messageHandler = (event: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    this.setState({ temp_message: value });
  };

  btnAddHandler = () => {
    const newInputList: ITodo = {
      input: this.state.temp_input,
      message: this.state.temp_message
    };
    this.setState(prevState => ({ todo: [...prevState.todo, newInputList] }));
    this.setState({ temp_input: "", temp_message: "" });
  };

  deleteHandler = (index: number) => {
    this.setState(prevState => ({
      todo: prevState.todo.filter((_, i) => i !== index)
    }));
  };

  editHandler = (index: number) => {
    this.setState ({editData: false});
    const {todo} = this.state;
    const editExistingData = todo.filter((item, idx) => idx === index);
    const editTitle = editExistingData.map((item)=>item.input);
    const editMessage = editExistingData.map((item)=> item.message);
    this.setState({temp_input: editTitle.toString()});
    this.setState({temp_message: editMessage.toString()});
    this.setState({index: index});

  };

  handleEditAction = () => {
    const updateData = {
      input: this.state.temp_input,
      message:this.state.temp_message,
    };

    const dataUpdated = this.state.todo.map((item, index)=> {
      if (index === this.state.index){
        return {...item, ...updateData};
      }
      return item;
    });
    this.setState({
      todo: dataUpdated,
      temp_input: "",
      temp_message: "",
      editData: true,
    });

  }

  checkHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    this.setState({ showDataTable: checked });
  };

  render() {
    const { showDataTable } = this.state;
    return (
      <div>
        <h1>Home Page</h1>
        <TodoForm
          inputHandler={this.inputHandler}
          messageHandler={this.messageHandler}
          btnAddHandler={this.btnAddHandler}
          temp_input={this.state.temp_input}
          temp_message={this.state.temp_message}
        />
        <br />
        <br />
        <label>
          <input
            type="checkbox"
            onChange={this.checkHandler}
            checked={showDataTable}
          />{" "}
          <span>Show Table</span>
        </label>
        <br />
        {showDataTable && (
          <TodoDisplay
            inputlist={this.state.todo}
            deleteHandler={this.deleteHandler}
            editHandler={this.editHandler}
          />
        )}
      </div>
    );
  }
}

export default Home;
