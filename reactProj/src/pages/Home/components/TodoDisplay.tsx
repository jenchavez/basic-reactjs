// TodoDisplay.tsx
import React from 'react';
import { ITodo, ITodoDisplayProp } from '../type';

const TodoDisplay: React.FC<ITodoDisplayProp> = ({ inputlist, deleteHandler, editHandler }) => {
  return (
    <>
      {!inputlist.length ? (<h5> No Todo</h5>) : (
        <table border={1}>
          <tbody>
            <tr>
              <th>Title</th>
              <th>Message</th>
              <th>Actions</th>
            </tr>
            {inputlist.map((items, index) => {
              return (
                <tr key={items.input + index}>
                  <td>{items.input}</td>
                  <td>{items.message}</td>
                  <td>
                    <button onClick={() => deleteHandler(index)}>Delete</button>
                    <button onClick={() => editHandler(index)}>Edit</button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      )}
    </>
  );
}

export default TodoDisplay;
