
export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    todo: ITodo[];
    showDataTable: boolean;
    editData: boolean;
    index: number;
}

export interface ITodo {
    input: string;
    message: string;
}

export interface ITodoDisplayProp {
    inputlist: ITodo[];
    deleteHandler: (index: number) => void;
    editHandler: (index: number) => void;
}
